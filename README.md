Use endpoints:

  - GET api/recommendations/random
  - GET api/recommendations/long_title
  - GET api/recommendations/even_w

for different recommendation algorithms.
