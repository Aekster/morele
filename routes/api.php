<?php

declare(strict_types=1);

use App\Http\Controllers\MovieController;
use Illuminate\Routing\Router;

/** @var Router $router */
$router = app(Router::class);

$router->get('/recommendations/{type}', [MovieController::class, 'recommend'])
    ->name('recommendations')
;
