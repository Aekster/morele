<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Enums\RecommendationType;
use App\Queries\GetMoviesQuery;
use App\Services\RecommendationService;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class RecommendationServiceTest extends TestCase
{
    protected MockObject $getMoviesQueryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->getMoviesQueryMock = $this
            ->getMockBuilder(GetMoviesQuery::class)
            ->getMock()
        ;
    }

    public function testRandomRecommendationReturnsExpectedCount(): void
    {
        include storage_path(GetMoviesQuery::MOVIES_LIST_PATH);
        $moviesData = $movies;

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::once())
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $recommendations = $service->getRecommendations(
            RecommendationType::RANDOM
        );

        self::assertCount(3, $recommendations);
    }

    public function testRandomRecommendationReturnsDifferentValues(): void
    {
        include storage_path(GetMoviesQuery::MOVIES_LIST_PATH);
        $moviesData = $movies;
        $callCount = 100;

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::exactly($callCount))
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $results = [];
        for ($i = 0; $i < $callCount; ++$i) {
            $recommendations = $service->getRecommendations(
                RecommendationType::RANDOM
            );
            $results[] = $recommendations;
        }

        self::assertTrue(
            \count(array_unique(array_map('json_encode', $results))) > 1
        );
    }

    public function testRandomRecommendationReturnsCorrectCountIfTooFewData(): void
    {
        $moviesData = [
            'Pulp Fiction',
        ];

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::once())
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $recommendations = $service->getRecommendations(
            RecommendationType::RANDOM
        );

        self::assertCount(1, $recommendations);
    }

    public function testLogTitleRecommendationReturnsExpectedData(): void
    {
        $moviesData = [
            'Pulp Fiction',
            'Django',
        ];

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::once())
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $recommendations = $service->getRecommendations(
            RecommendationType::LONG_TITLE
        );

        self::assertSame(['Pulp Fiction'], $recommendations);
    }

    public function testLogTitleRecommendationIgnoresCompounds(): void
    {
        $moviesData = [
            'Sin-City',
            'Pulp Fiction',
        ];

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::once())
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $recommendations = $service->getRecommendations(
            RecommendationType::LONG_TITLE
        );

        self::assertSame(['Pulp Fiction'], $recommendations);
    }

    public function testStartsWithWRecommendationReturnsCorrectData(): void
    {
        $moviesData = [
            'Pulp Fiction',
            'Django',
            'Whiplash',
            'Wyspa tajemnic',
            'Wielki Gatsby',
        ];

        $service = new RecommendationService($this->getMoviesQueryMock);

        $this->getMoviesQueryMock->expects(self::once())
            ->method('getData')
            ->willReturn($moviesData)
        ;

        $recommendations = $service->getRecommendations(
            RecommendationType::EVEN_W
        );

        self::assertSame(['Whiplash', 'Wyspa tajemnic'], $recommendations);
    }
}
