<?php

declare(strict_types=1);

namespace App\Enums;

enum RecommendationType: string
{
    case RANDOM = 'random';
    case LONG_TITLE = 'long_title';
    case EVEN_W = 'even_w';
}
