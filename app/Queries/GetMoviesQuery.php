<?php

declare(strict_types=1);

namespace App\Queries;

class GetMoviesQuery
{
    public const MOVIES_LIST_PATH = 'app/movies.php';

    public function getData(): array
    {
        include storage_path(self::MOVIES_LIST_PATH);

        return $movies;
    }
}
