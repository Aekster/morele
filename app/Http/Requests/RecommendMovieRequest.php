<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RecommendationType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class RecommendMovieRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge(['type' => $this->route('type')]);
    }

    public function rules(): array
    {
        return [
            'type' => [
                'required',
                new Enum(RecommendationType::class),
            ],
        ];
    }
}
