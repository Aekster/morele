<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\RecommendationType;
use App\Http\Requests\RecommendMovieRequest;
use App\Http\Responses\ApiResponse;
use App\Services\RecommendationService;
use Illuminate\Http\JsonResponse;

class MovieController extends Controller
{
    private RecommendationService $movieService;

    public function __construct(RecommendationService $movieService)
    {
        $this->movieService = $movieService;
    }

    public function recommend(RecommendMovieRequest $request, ApiResponse $apiResponse): JsonResponse
    {
        $recommendationType = RecommendationType::tryFrom($request->get('type'));

        $recommendedMovies = $this->movieService->getRecommendations(
            $recommendationType
        );

        return $apiResponse
            ->addData($recommendedMovies)
            ->addMessage($this->getTypeSuccessMessage($recommendationType))
            ->create()
        ;
    }

    protected function getTypeSuccessMessage(RecommendationType $recommendationType): string
    {
        return trans('movie.recommendations.'.$recommendationType->value);
    }
}
