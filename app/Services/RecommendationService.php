<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\RecommendationType;
use App\Queries\GetMoviesQuery;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class RecommendationService
{
    public const RANDOM_RECOMMENDED_COUNT = 3;
    public const STARTING_LETTER = 'W';
    public const WORD_COUNT = 2;
    private GetMoviesQuery $getMoviesQuery;

    public function __construct(GetMoviesQuery $getMoviesQuery)
    {
        $this->getMoviesQuery = $getMoviesQuery;
    }

    public function getRecommendations(RecommendationType $recommendationType): array
    {
        $movieTitles = collect($this->getMovieTitles());

        return match ($recommendationType) {
            RecommendationType::RANDOM => $this->recommendRandom(
                $movieTitles,
                self::RANDOM_RECOMMENDED_COUNT
            ),
            RecommendationType::LONG_TITLE => $this->recommendLongTitle(
                $movieTitles,
                self::WORD_COUNT
            ),
            RecommendationType::EVEN_W => $this->recommendEvenW(
                $movieTitles,
                self::STARTING_LETTER
            )
        };
    }

    protected function getMovieTitles(): array
    {
        return $this->getMoviesQuery->getData();
    }

    protected function recommendRandom(Collection $movieTitles, int $count): array
    {
        $count = ($count < $movieTitles->count()) ? $count : $movieTitles->count();

        return $movieTitles->random($count)->values()->toArray();
    }

    protected function recommendLongTitle(Collection $movieTitles, int $wordCount): array
    {
        return $movieTitles->filter(static function ($movieTitle) use ($wordCount) {
            $words = explode(' ', $movieTitle);

            return \count($words) >= $wordCount;
        })->values()->toArray();
    }

    protected function recommendEvenW(Collection $movieTitles, string $startingLetter): array
    {
        return $movieTitles->filter(static function ($movieTitle) use ($startingLetter) {
            return Str::startsWith($movieTitle, $startingLetter)
                && 0 === \strlen($movieTitle) % 2;
        })->values()->toArray();
    }
}
