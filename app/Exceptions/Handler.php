<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Http\Responses\ApiResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(static function (\Throwable $exception, $request) {
            $apiResponse = app(ApiResponse::class);

            if ($request->is('api/*')) {
                if ($exception instanceof HttpException) {
                    return $apiResponse
                        ->setCode($exception->getStatusCode())
                        ->addMessage($exception->getMessage())
                        ->create()
                    ;
                }

                if ($exception instanceof ValidationException) {
                    return $apiResponse
                        ->setCode(Response::HTTP_UNPROCESSABLE_ENTITY)
                        ->addMessage($exception->getMessage())
                        ->addErrors($exception->errors())
                        ->create()
                    ;
                }
            }
        });
    }
}
