<?php

declare(strict_types=1);

use App\Enums\RecommendationType;

return [
    'recommendations' => [
        RecommendationType::RANDOM->value => 'We recommend 3 random titles.',
        RecommendationType::LONG_TITLE->value => 'We recommend all the movies that have a title made of at least two words.',
        RecommendationType::EVEN_W->value => 'We recommend all the movies that have a title that starts with W and have and even number of characters.',
    ],
];
