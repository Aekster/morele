<?php

declare(strict_types=1);

use PhpCsFixer\Config;
use Symfony\Component\Finder\Finder;

$finder = (new Finder())
    ->ignoreDotFiles(false)
    ->ignoreVCSIgnored(true)
    ->exclude(['vendor', 'storage'])
    ->in(__DIR__)
;

return (new Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PHP74Migration' => true,
        '@PHP74Migration:risky' => true,
        '@PHPUnit100Migration:risky' => true,
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        'general_phpdoc_annotation_remove' => ['annotations' => ['expectedDeprecation']],
        'modernize_strpos' => true,
        'no_useless_concat_operator' => false,
        'numeric_literal_separator' => true,
    ])
    ->setFinder($finder)
;
